module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({

    files: {
      js: [ 'Gruntfile.js', 'src/**/*.js', 'spec/**/*Spec.js' ]
    },

    jasmine: {
      gaia: {
        src: 'src/**/*.js',

        options: {
          keepRunner: true,
          specs: 'spec/**/*Spec.js',
          helpers: 'spec/helper.js'
        }
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },

      gaia: '<%= files.js %>'
    },

    watch: {
      js: {
        files: '<%= files.js %>',
        tasks: 'test'
      }
    },

    groundskeeper: {
      build: {
        files: {
          'build/gaia.js': 'src/gaia.js'
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-groundskeeper');

  grunt.registerTask('lint', 'jshint');
  grunt.registerTask('test', 'jasmine');
  grunt.registerTask('default', 'watch:js');

};
