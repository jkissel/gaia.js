(function () {
  'use strict';

  // Polyfill
  // --SNIP--

  var proto = Element.prototype;

  proto.matches = (proto.matches || proto.mozMatchesSelector);

  // --SNAP--

  var global = window,
      previousGaia = global.gaia,
      gaia = global.gaia = {},
      util = gaia.util = {};

  gaia.noConflict = function () {
    global.gaia = previousGaia;
    return gaia;
  };

  gaia.i18n = {
    cancel: 'Cancel',
    ok: 'OK',
    done: 'done'
  };

  util.createElement = function (tagName, attributes, content, html) {
    var el = document.createElement(tagName);

    if (isMap(attributes)) {
      Object.keys(attributes).forEach(function (name) {
        el.setAttribute(name, attributes[name]);
      });
    } else {
      html = content;
      content = attributes;
    }

    if (content) {
      if (Array.isArray(content)) {
        content.forEach(function (child) {
          el.appendChild(child);
        });
      } else {
        el[html ? 'innerHTML' : 'textContent'] = content;
      }
    }

    return el;
  };

  util.tr = function (msgid) {
    return (gaia.i18n[msgid] || msgid);
  };

  util.extend = function (target, source) {
    return Object.keys(source).reduce(function (target, key) {
      return (target[key] = source[key], target);
    }, target);
  };

  util.pick = function (obj) {
    return [].slice.call(arguments, 1).reduce(function (res, key) {
      return (res[key] = obj[key], res);
    }, {});
  };

  util.defaults = function defaults(obj, source) {
    obj = (obj || {});

    if (source) {
      for (var prop in source) {
        var val = obj[prop], def = source[prop];

        if (isMap(val) && isMap(def)) {
          obj[prop] = defaults(val, def);
        } else if (isUndefined(val)) {
          obj[prop] = def;
        }
      }
    }

    return obj;
  };

  function isMap(val) {
    return (Object.prototype.toString.call(val) === '[object Object]');
  }

  function isUndefined(val) {
    return (typeof val === 'undefined');
  }

  function isFunction(val) {
    return (typeof val === 'function');
  }

  util.isMap = isMap;
  util.isUndefined = isUndefined;
  util.isFunction = isFunction;

  util.on = function (el, selector, event, listener) {
    if (arguments.length < 4) {
      listener = event;
      event = selector;
      selector = false;
    }

    el.addEventListener(event, function (evt) {
      if (! selector || evt.target.matches(selector)) {
        evt.preventDefault();
        listener(evt);
      }
    });
  };

  util.$ = function (selector, el) {
    if (! el) {
      el = document;
    }

    return el.querySelector(selector);
  };

})();

(function (gaia) {
  'use strict';

  var util = gaia.util,
      el = util.createElement;

  function parseOptions(options) {
    if (options && ! util.isMap(options.submit)) {
      options.submit = { text: options.submit };
    }

    return util.defaults(options, {
      cancel: util.tr('cancel'),
      submit: { text: util.tr('ok'), class: 'recommend' }
    });
  }

  function create(options) {
    var section = el('section', [ el('p', options.message, options.html) ]);

    if (options.title) {
      section.insertBefore(el('h1', options.title, options.html), section.firstChild);
    }

    return el('form', { role: 'dialog', 'data-type': 'confirm' }, [
      section,

      el('menu', [
        el('button', { 'data-id': 'cancel' }, options.cancel),
        el('button', { 'data-id': 'submit', class: options.submit.class }, options.submit.text)
      ])
    ]);
  }

  gaia.confirm = function (options, callback) {
    var el = create(parseOptions(options));

    util.on(util.$('menu', el), 'button', 'click', function (evt) {
      el.remove();
      callback(evt.target.dataset.id !== 'cancel');
    });

    document.body.appendChild(el);
  };

})(gaia);

(function (gaia) {
  'use strict';

  var util = gaia.util,
      el = util.createElement,
      tr = util.tr;

  function parseOptions(options) {
    return util.defaults(options, {
      cancel: tr('cancel'),
      actions: []
    });
  }

  function create(options) {
    return el('form', { role: 'dialog', 'data-type': 'action' }, [
      el('header', options.title, options.html),

      el('menu', options.actions.map(function (action, i) {
        if (! util.isMap(action)) { action = { text: action }; }
        var attributes = util.extend(util.pick(action, 'class', 'disabled'), { 'data-id': action.name || i });
        return el('button', attributes, action.text);
      }).concat(el('button', { 'data-id': 'cancel' }, options.cancel)))
    ]);
  }

  gaia.action = function (options, callback) {
    var el = create(parseOptions(options));

    util.on(util.$('menu', el), 'button', 'click', function (evt) {
      el.remove();

      var id = evt.target.dataset.id,
          idx = parseInt(id, 10);

      callback(isNaN(idx) ? id : idx);
    });

    document.body.appendChild(el);
  };

})(gaia);

(function (gaia) {
  'use strict';

  var util = gaia.util,
      $ = util.$;

  function parseOptions(options) {
    return util.defaults(options, { right: false, duration: 0.4 });
  }

  gaia.page = function (selector, options) {
    var currentPage = $('[data-position=current]');

    if (selector) {
      options = parseOptions(options);

      var page = $(selector),
          left = (page.dataset.position !== 'right' && ! options.right);

      if (left) {
        animate(currentPage, 'currentToLeft');
        animate(page,        'rightToCurrent');
      } else {
        animate(currentPage, 'currentToRight');
        animate(page,        'leftToCurrent');
      }

      delete currentPage.dataset.position;
      page.dataset.position = 'current';

      return page;
    } else {
      return currentPage;
    }

    function animate(page, transition) {
      page.style.animation = [ transition, options.duration + 's', 'forwards' ].join(' ');
    }
  };

})(gaia);
