describe('gaia', function () {
  'use strict';

  describe('noConflict()', function () {

    var gaia = window.gaia;

    afterEach(function () {
      window.gaia = gaia;
    });

    it('should restore the previous window.gaia', function () {
      expect(window.gaia).not.toBe(window._gaia);
      gaia.noConflict();
      expect(window.gaia).toBe(window._gaia);
    });

    it('should return the gaia instance', function () {
      expect(gaia.noConflict()).toBe(gaia);
    });

  });

  describe('util', function () {

    describe('defaults()', function () {

      it('should create an object, if none was given', function () {
        var defaults = gaia.util.defaults,
            isMap = gaia.util.isMap;

        expect(isMap(defaults())).toBe(true);
        expect(isMap(defaults(null, {}))).toBe(true);
      });

      it('should fill in undefined properties (and only these)', function () {
        var defaults = gaia.util.defaults;

        expect(defaults({},             { test: 1 }).test).toBe(1);
        expect(defaults({ test: 1 },    { test: 2 }).test).toBe(1);
        expect(defaults({ test: null }, { test: 2 }).test).toBe(null);
      });

      it('should do recursion on object properties', function () {
        var defaults = gaia.util.defaults,
            res = defaults({ test: 1 }, { test: 2, foo: 1 });

        expect(res.test).toBe(1);
        expect(res.foo).toBe(1);
      });

    });

    describe('tr()', function () {

      it('should lookup translations in gaia.i18n map', function () {
        expect(gaia.util.tr('ok')).toBe(gaia.i18n.ok);
      });

      it('should return the given msgid if no translation was found', function () {
        expect(gaia.util.tr('brmpfts')).toBe('brmpfts');
      });

    });

    describe('el()', function () {

      it('should return an element with the given tag name', function () {
        var res = gaia.util.createElement('div');

        expect(typeof res).toBe('object');
        expect(res instanceof HTMLElement).toBe(true);
        expect(res.tagName).toBe('DIV');
      });

      it('should set the given attributes', function () {
        var res = gaia.util.createElement('div', {
          'id': 'id',
          'data-id': 'test',
          'class': 'foo bar'
        });

        expect(res.id).toBe('id');
        expect(res.dataset.id).toBe('test');
        expect(res.classList.length).toBe(2);
      });

      it('should set the given string as textContent', function () {
        var el = gaia.util.createElement;

        expect(el('div', {}, 'Test').textContent).toBe('Test');
        expect(el('div', {}, '<strong>Test</strong>').textContent).toBe('<strong>Test</strong>');
      });

      it('should treat the attributes argument as being optional', function () {
        expect(gaia.util.createElement('div', 'Test').textContent).toBe('Test');
      });

      it('should set the given string as innerHTML, if the html attribute is true', function () {
        var res = gaia.util.createElement('div', '<strong>Test</strong>', true);

        expect(res.textContent).toBe('Test');
        expect(res.innerHTML).toBe('<strong>Test</strong>');
      });

      it('should append the given array of children', function () {
        var el = gaia.util.createElement;

        expect(el('p', [ el('span'), el('span') ]).childNodes.length).toBe(2);
      });

    });

    describe('isMap()', function () {

      it('should only return true for simple objects (no special prototype)', function () {
        var isMap = gaia.util.isMap,
            negative = [ [], null, 1 ];

        expect(isMap({})).toBe(true);
        expect(negative.some(isMap)).toBe(false);
      });

    });

    describe('isUndefined()', function () {

      it('should only return true for undefined values', function () {
        var isUndefined = gaia.util.isUndefined,
            negative = [ {}, null, 1 ];

        expect(isUndefined(undefined)).toBe(true);
        expect(negative.some(isUndefined)).toBe(false);
      });

    });

  });

  describe('confirm()', function () {

    describe('_parseOptions', function () {
      gaia.confirm._parseOptions();
    });

  });

});
